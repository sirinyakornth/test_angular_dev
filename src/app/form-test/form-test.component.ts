import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-test',
  templateUrl: './form-test.component.html',
  styleUrls: ['./form-test.component.scss']
})
export class FormTestComponent implements OnInit {
  validateForm!: FormGroup;
  formList = [
    {
      id: "name",
      value: "test",
      type: "text"
    },
    {
      id: "number",
      value: "3",
      type: "number"
    },
    {
      id: "email",
      value: "",
      type: "text"
    },
    {
      id: "gender",
      value: "male",
      type: "radio"
    },
    {
      id: "gender3",
      value: "female",
      type: "checkbox"
    },
    {
      id: "code",
      value: "code1",
      type: "select"
    },
    {
      id: "comment",
      value: "textarea",
      type: "textarea"
    }
  ]
  // typeInput : any;
  typeInput = [
    {
      type: "text"
    },
    {
      type: "number"
    },
    {
      type: "radio"
    },
    {
      type: "checkbox"
    },
    {
      type: "select"
    },
    {
      type: "textarea"
    }
  ]
  chkChngForm: boolean = false;
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      id: [null, [Validators.required]],
      value: [null, [Validators.required]],
      type: [null, [Validators.required]],
    });
    // this.typeInput = this.formList.map(ele => {
    //   return ele.type
    // })
    console.log("this.typeInput", this.typeInput);

  }
  changeForm() {
    this.chkChngForm = true;
  }
  closeChangeForm() {
    this.chkChngForm = false;

  }
  submitForm() {
    console.log("validateForm",this.validateForm);
    if(this.validateForm.valid){
      this.formList.push(this.validateForm.value)
    }
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormTestComponent } from './form-test.component';
import { NgZorroAntdModule } from '../ng-zorro-antd.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    FormTestComponent
  ],
  imports: [
    CommonModule,
    NgZorroAntdModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [FormTestComponent]
})
export class FormTestModule { }
